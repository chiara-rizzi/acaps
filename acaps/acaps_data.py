"""
Utilities to retrieve and store ACAPS data from with the ACAPS python API
"""

import pandas as pd
import requests
from datetime import datetime, date
import time
from dateutil.relativedelta import relativedelta
from pathlib import Path
from getpass import getpass

def get_list_months(n_months=18):
    """
    List of the last n_months months in the format needed 
    to retrieve the acaps endpoint: MMMYY
    """
    list_months = pd.date_range(date.today() - relativedelta(months=n_months),date.today(), 
                  freq='MS').strftime("%b%Y").tolist()
    return list_months

def get_credentials(username=None, password=None):
    """
    Check if username and password are available, otherwise ask them
    """
    if username is None:
        username = input('Enter ACAPS API username: ')
    if password is None:
        password = getpass('Enter ACAPS API password: ')
    return username, password


def get_df(endpoint, username=None, password=None, cap_1000=False):
    """
    Function to retrieve a datased from api.acaps.org starting from the 
    name of the endpoint, the username and the password
    """

    username,password = get_credentials(username, password)
    
    credentials = {
        "username": username,
        "password": password 
    }
    
    auth_token_response = requests.post("https://api.acaps.org/api/v1/token-auth/", credentials)
    auth_token = auth_token_response.json()['token']

    # Pull data from ACAPS API, loop through the pages, and append to a pandas DataFrame
    df = pd.DataFrame()
    request_url = "https://api.acaps.org/"+endpoint
    # make sure format is ok
    request_url = request_url.replace('org//','org/')
    print('reading from...')
    print(request_url)
    last_request_time = datetime.now()
    while True:
        # Wait to avoid throttling
        while (datetime.now()-last_request_time).total_seconds() < 1:
            time.sleep(0.1)             
        # Make the request
        response = requests.get(request_url, headers={"Authorization": "Token %s" % auth_token})
        last_request_time = datetime.now()
        response = response.json()        
        # Append to a pandas DataFrame
        df = df.append(pd.DataFrame(response["results"]))        
        # Loop to the next page; if we are on the last page, break the loop
        if ("next" in response.keys()) and (response["next"] != None):
            request_url = response["next"]
        else: break
        if cap_1000 and df.shape[0]>1000: break # if you are testing just to make sure you can read the file
    return df

def get_csv_name(endpoint):
    """
    Build csv name from the endpoint name. 
    Same name as the endpoint, but replacing '/' with '-'.
    """
    return endpoint.strip('/').replace('/','-')+'.csv'

def write_csv(endpoint, username, password, outdir='./', n_months=-1):
    """
    Get the data and store it as a csv file
    """
    username, password = get_credentials(username, password)
    csv_name = get_csv_name(endpoint)
    endpoint = '/'+endpoint.strip('/')+'/'
    if n_months>0:
        list_months = get_list_months(n_months)
        df = pd.DataFrame()
        for month in list_months:  
            df_tmp = get_df(endpoint+month, username, password)
            df_tmp['month'] = datetime.strptime(month, "%b%Y") # month as datetime for easier handling
            df = df.append(df_tmp)
    else:
        df = get_df(endpoint, username, password)
    Path(outdir).mkdir(parents=True, exist_ok=True)
    df.to_csv(outdir+'/'+csv_name)
    return

def read_or_get_csv(endpoint, indir, username=None, password=None, n_months=-1):
    """
    Read or download the csv file and rerurn the dataframe
    """
    csv_name = get_csv_name(endpoint)
    csv_path = Path(indir+'/'+csv_name)
    if csv_path.exists():
        df = pd.read_csv(csv_path)
    else: # download the data
        username, password = get_credentials(username, password)
        write_csv(endpoint, username, password, outdir=indir, n_months=n_months)        
        df = pd.read_csv(csv_path)
    return df


if __name__ == '__main__':
    print('test')
    df = get_df("api/v1/humanitarian-access/Nov2021", None, None)



