"""
Script to get all the files needed for the analysis
"""

import argparse
import json 
from pathlib import Path

import sys
sys.path.append('../../')
# library to handle the ACAPS API, with several functionalities to return data 
from acaps import acaps_data

def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--outdir', '-o', default='../../data/raw', type=str,
                        help='Path to the output directory for the figures')
    parser.add_argument('--config', '-c', default='all_endpoints.json', type=str,
                        help='Configuration file with endpoin names')
    parser.add_argument('--username', '-u', default=None, type=str,
                        help='ACAPS API username')
    parser.add_argument('--password', '-p', default=None, type=str,
                        help='ACAPS API password')
    return parser.parse_args()

def main():
    args = parse_args()
    # create the output directory if it doesn't exist
    Path(args.outdir).mkdir(parents=True, exist_ok=True)
    with open(args.config) as config_file:
        config = json.load(config_file)
    for end in config['endpoints']:
        acaps_data.write_csv(end[0], args.username, args.password, args.outdir, int(end[1]))

if __name__ == '__main__':
    main()
