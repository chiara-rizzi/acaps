"""
Run the analysis and visualization of the INFORM Severity Index. 
The input and output directories names are configurable, 
the endpoint names are hardcoded based on the needed information. 
"""

import argparse
import math
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
import plotly.figure_factory as ff
from plotly.subplots import make_subplots
from pathlib import Path
import datetime
from dateutil.relativedelta import relativedelta

import sys
sys.path.append('../../')
# library to handle the ACAPS API, with several functionalities to return data 
from acaps import acaps_data

# plotly as default pandas plotting backend
pd.options.plotting.backend = 'plotly'


def parse_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--outdir', '-o', default='./', type=str,
                        help='Path to the output directory for the figures')
    parser.add_argument('--indir', '-i', default='../../data/raw/', type=str,
                        help='Path to the output directory for the figures')
    parser.add_argument('--png', action='store_true',
                        help='Store png file for the visualizations (the default is to store html only)')
    parser.add_argument('--pdf', action='store_true',
                        help='Store pdf file for the visualizations (the default is to store html only)')
    parser.add_argument('--username', '-u', default=None, type=str,
                        help='ACAPS API username')
    parser.add_argument('--password', '-p', default=None, type=str,
                        help='ACAPS API password')
    list_of_choices = ['all', 'map', 'corr', 'evolving-crises', 'indicators']
    parser.add_argument('--vis','-v' , 
                        choices=list_of_choices, nargs='*', default=[])
    args = parser.parse_args()
    return args


def write_fig(fig, name, outdir, do_png, do_pdf):
    """
    Write the plotly figure to html and, if requested, png and pdf
    """
    # create the output directory if it doesn't exist
    Path(outdir).mkdir(parents=True, exist_ok=True)
    # write the html file (default)
    fig.write_html(outdir+'/'+name+'.html', full_html=False, include_plotlyjs='cdn')    
    # write the png file (if requested)
    if do_png:
        fig.write_image(outdir+'/'+name+'.png')
    if do_pdf:
        fig.write_image(outdir+'/'+name+'.pdf')
    return


def plot_map_index(indir, outdir, do_png=False, do_pdf=False, username=None, password=None):
    """
    Plot the geographical distribution of the 
    INFORM Severity Index
    """    
    endpoint = '/api/v1/inform-severity-index/'
    df = acaps_data.read_or_get_csv(endpoint, indir, username, password, n_months=18)
    # when there are multiple countries, I want to show only one marker --> position it on the first country of the list
    df['one_iso3'] = df['iso3'].apply(lambda x: x.strip('[').strip(']').split(',')[0].strip('\''))     # when I read from csv, lists become strings
    df['country'] = df['country'].apply(lambda x: x.strip('[').strip(']').replace('\'',''))
    fig = px.scatter_geo(df[~ np.isnan(df['INFORM Severity Index'])], locations="one_iso3", color="INFORM Severity Index",
                         hover_name="crisis_name", 
                         hover_data=['country'],
                         size="INFORM Severity Index", size_max=12,
                         projection="natural earth", animation_frame="month", animation_group='crisis_id',
                         color_continuous_scale=px.colors.sequential.Bluered,
                         labels={'country':'Country','one_iso3':'Code of first country','month':'Month',
                                 'INFORM Severity Index':'INFORM<br>Severity Index'})
    # same range of z axis for all figures
    fig.update_coloraxes(cmin=0.01, cmax=4.99)
    # reach the configuration of the play button to slow down animation
    fig.layout.updatemenus[0].buttons[0].args[1]["frame"]["duration"] = 1000
    write_fig(fig, 'map', outdir, do_png, do_pdf)
    return


def get_list_evolving_crises(df, threshold=1.0):
    df_pivot = df.pivot(index='month', columns='crisis_id')['INFORM Severity Index']
    # remove columns where there's ony one unique value (it also removes all-NaN columns)
    # or where the change is small
    for c in df_pivot.columns:
        if df_pivot[c].max() - df_pivot[c].min() < 1 or  len(df_pivot[c].unique()) == 1:
            df_pivot.drop(c,inplace=True,axis=1)
    return list(df_pivot.columns)    


def plot_evolving_crises(indir, outdir, do_png=False, do_pdf=False, username=None, password=None):
    """
    Plot the temporal evolution of 
    the crises with a large chance in INFORM severity index
    """    
    endpoint = '/api/v1/inform-severity-index/'
    df = acaps_data.read_or_get_csv(endpoint, indir, username, password, n_months=18)
    evolving_crises = get_list_evolving_crises(df, threshold=1)
    df_sel = df[df['crisis_id'].isin(evolving_crises)]
    fig = px.line(df_sel, x='month', y='INFORM Severity Index', color='crisis_id', 
                  hover_name='crisis_name', hover_data=['country','month','INFORM Severity Index'],
                  title='Evolution of INFORM Severity Index for crises with a large change',
                  markers=True, line_dash='crisis_id',
                  labels={'crisis_id':'Crisis ID','month':'Month'})
    write_fig(fig, 'deteriorating_crises', outdir, do_png, do_pdf)
    return

def plot_correlation_with_access(indir, outdir, do_png=False, do_pdf=False, username=None, password=None):
    """
    Compute and visualize the correlation between 
    the INFORM Severity Index and the Humanitarian ACCESS Score
    """
    endpoint = '/api/v1/inform-severity-index/'
    df = acaps_data.read_or_get_csv(endpoint, indir, username, password, n_months=18)
    # ACCESS information
    endpoint_access = '/api-v1-humanitarian-access/'
    df_access = acaps_data.read_or_get_csv(endpoint_access, indir, username, password, n_months=18)
    # Merge the two dataframes
    df_crisis = pd.merge(df[['crisis_id','crisis_name','country','regions','INFORM Severity Index','month',
                             'Impact of the crisis','Conditions of affected people','Complexity']],
                         df_access[['crisis_id','ACCESS','month']], on=['crisis_id','month'])
    # compute correlation
    corr = df_crisis.corr().loc['INFORM Severity Index','ACCESS']
    # plot
    fig = px.scatter(df_crisis, x='INFORM Severity Index', y = 'ACCESS',hover_name="crisis_name", 
                     hover_data=['month'],
                     marginal_x='histogram', marginal_y='histogram', 
                     title=f"Access vs Severity Index. Correlation: {corr:.2f}")
    # update markers to be trnasparent, to show where there are several points overlapping
    fig.update_traces(marker=dict(opacity=0.4, size=12, #color='#f5a851',
                                  line=dict(width=1, color='DarkSlateGrey')),
                      selector=dict(mode='markers'))    
    write_fig(fig, 'correlation', outdir, do_png, do_pdf)

    interesting_col = ["INFORM Severity Index","Impact of the crisis", "Conditions of affected people","Complexity", "ACCESS" ]
    labels={"INFORM Severity Index":'INFORM<br>Severity<br>Index', "Impact of the crisis":"Impact<br>of crisis", 
            "Conditions of affected people":"Conditions<br>of people",
            "Complexity":"Complexity<br>of crisis",
            "ACCESS":"ACCESS<br>score"}
    fig = px.scatter_matrix(df_crisis, dimensions=interesting_col, labels=labels)
    fig.update_layout( autosize=True, height=600)
    write_fig(fig, 'correlation-comp', outdir, do_png, do_pdf)

    corr = df_crisis[interesting_col].corr()
    corr_text = np.around(corr.values, decimals=2)
    ff_fig = ff.create_annotated_heatmap(corr_text, x=list(corr.columns), 
                                         y=list(corr.columns), showscale = True,
                                         colorscale=px.colors.sequential.Purples)
    fig  = go.FigureWidget(ff_fig)
    fig['layout']['xaxis'].update(side='bottom')
    fig.update_layout( autosize=True, height=400)
    write_fig(fig, 'correlation-map', outdir, do_png, do_pdf)

    # look at the correlation with the complexity components
    endpoint_compl = '/api/v1/inform-severity-index/complexity'
    df_compl = acaps_data.read_or_get_csv(endpoint_compl, indir, username, password, n_months=18)
    compl_components = ['Diversity of groups affected', 'Humanitarian access','Rule of Law','Safety and security',
                        'Social cohesion']
    df_compl_access = pd.merge(df_compl[compl_components+['month','crisis_id']], df_access[['crisis_id','ACCESS','month']], on=['crisis_id','month'])
    
    corr = df_compl_access[compl_components+['ACCESS']].corr()
    corr_text = np.around(corr.values, decimals=2)
    ff_fig = ff.create_annotated_heatmap(corr_text, x=list(corr.columns), 
                                         y=list(corr.columns), showscale = True,
                                         colorscale=px.colors.sequential.Purples)
    fig  = go.FigureWidget(ff_fig)
    fig['layout']['xaxis'].update(side='bottom')
    fig.update_layout( autosize=True, height=400)
    write_fig(fig, 'correlation-map-complexity', outdir, do_png, do_pdf)
    
    return

def shorten_name(ind):
    """
    Simple function to shorten names of indicators when too long. 
    Used in plot_indicators. 
    """
    ind = ind.replace('Minimal humanitarian conditions','Conditions')
    ind = ind.replace('Moderate humanitarian conditions','Conditions')
    ind = ind.replace('Severe humanitarian conditions','Conditions')
    ind = ind.replace('Extreme humanitarian conditions','Conditions')
    ind = ind.replace('Stressed humanitarian conditions','Conditions')
    return ind

def create_ts_dictionary(df_log, evolving_crises, is_country=False):
    """
    Simple function to create dictionary of time series. 
    Used in plot_indicators
    """
    array_indicators = df_log['indicator'].unique().astype(str)
    array_indicators = array_indicators[array_indicators != 'nan']
    all_indicators = list(array_indicators)
    ts_log_dict = {}
    date_name = 'date' if is_country else 'date_of_entry'
    value_name = 'value' if is_country else 'figure'
    for cr_id in evolving_crises:
        ts_log_dict[cr_id] = {}    
        for ind in all_indicators:
            ind_short = shorten_name(ind)
            if is_country:
                ts_log_dict[cr_id][ind_short] = df_log[(df_log['iso3']==cr_id[:3]) & (df_log['indicator']==ind)][[value_name,date_name]]
            else:
                ts_log_dict[cr_id][ind_short] = df_log[(df_log['crisis_id']==cr_id) & (df_log['indicator']==ind)][[value_name,date_name]]        
            ts_log_dict[cr_id][ind_short].set_index(date_name, inplace=True)
            ts_log_dict[cr_id][ind_short].index = pd.to_datetime(ts_log_dict[cr_id][ind_short].index)
            # set dates to the first of the month
            # this is for comparison with the released index of the month, which appears in the plots on the first of the month
            ts_log_dict[cr_id][ind_short].index = ts_log_dict[cr_id][ind_short].index.map(lambda x: x.replace(year=x.year, month=x.month, day=1))
            ts_log_dict[cr_id][ind_short] = ts_log_dict[cr_id][ind_short][value_name].dropna()
            if ts_log_dict[cr_id][ind_short].size < 1: # making sure there's at least one entry
                del ts_log_dict[cr_id][ind_short]
    return ts_log_dict

def create_indicators_dictionary(ts_log_dict, evolving_crises, country_log=False):
    """
    Simple function to create dictionary of indicators. 
    Used in plot_indicators
    """
    list_impact = ['Landmass affected', 'People exposed', 'People affected', 'People displaced', 'Fatalities reported']
    #list_complexity = ['Conflict Intensity (HIIK)', 'Income Gini Coefficient','Fatalities in all crises']
    indicators={}
    for cr_id in evolving_crises:
        # indicators available for this specific crisis 
        indicators[cr_id]={}
        indicators[cr_id]['all'] = sorted(ts_log_dict[cr_id].keys())
        if country_log:
            indicators[cr_id]['HIIK'] = [i for i in indicators[cr_id]['all'] if 'HIIK' in i]
            indicators[cr_id]['Gini'] = [i for i in indicators[cr_id]['all'] if 'Gini' in i]
            indicators[cr_id]['Complexity'] = [i for i in indicators[cr_id]['all'] if 'HIIK' in i or 'Gini' in i or 'Fatalities in all crises' in i]
        else:
            indicators[cr_id]['Conditions'] = [i for i in indicators[cr_id]['all'] if 'Conditions' in i or 'People in Need' in i]
            indicators[cr_id]['Impact'] = [i for i in indicators[cr_id]['all'] if i in list_impact]
            # sort according to list_impact (in this case there's a preferred order)
            indicators[cr_id]['Impact'] = [i for i in list_impact if i in indicators[cr_id]['Impact']]
    return indicators

def produce_plot_indicators(df, ts_log_dict, indicators):
    """
    Actual call to plotly to produce the indicator plots. 
    Used in plot_indicators
    """
    # pltly express does not support subplots, so in this case I'm using plotly graph objects
    time0 = datetime.datetime.today() - relativedelta(months=18)
    time1 = datetime.datetime.today()
    n_col = min(3, len(indicators))    
    n_row = math.ceil(len(indicators)/n_col)
    fig = make_subplots(rows=n_row, cols=n_col, start_cell="top-left", 
                    specs=[[{"secondary_y": True} for i in range(n_col)] for j in range(n_row)],
                   subplot_titles=[shorten_name(ind) for ind in indicators])
    for i,ind in enumerate(indicators):
        i_col = i%n_col + 1
        i_row = i//n_col +1

        hovertemplate='Value: %{y}<br>%{x|%B %Y}'
        
        fig.add_trace(go.Scatter(
            x = df['month'],
            y = df['INFORM Severity Index'], showlegend=False,
            name='INFORM Severity Index',
            line=dict(width=2,color='DarkSlateGrey'),
            hovertemplate=hovertemplate),
                      secondary_y=True, row=i_row, col=i_col)        

        palette = px.colors.qualitative.Plotly
        color = palette[i%len(palette)]        
        fig.add_trace(go.Scatter(x=ts_log_dict[ind].index, 
                                 y=ts_log_dict[ind], name=shorten_name(ind),
                                 line=dict(width=2,color=color),
                                 marker=dict(size=11,color=color, symbol='x'),
                                 hovertemplate=hovertemplate),
                      row=i_row, col=i_col)
        # update axis to last 18 months
        fig.update_xaxes(row=i_row, col=i_col, range=[time0, time1])
        # Set y-axes titles
        fig.update_yaxes(row=i_row, col=i_col, title_text="Indicator", secondary_y=False, title_standoff = 0, color=color)
        fig.update_yaxes(row=i_row, col=i_col, title_text="INFORM Severity", secondary_y=True, title_standoff = 0)

    fig.update_layout(font=dict(size=10))
    fig.update_layout(showlegend=False)
    fig.update_layout(height=200*(n_row+0.5))
    return fig

def plot_indicators(indir, outdir, do_png=False, do_pdf=False, username=None, password=None):
    """
    Plot indicators for 
    the crises with a large chance in INFORM severity index
    """    
    endpoint = '/api/v1/inform-severity-index/'
    df = acaps_data.read_or_get_csv(endpoint, indir, username, password, n_months=18)
    evolving_crises = get_list_evolving_crises(df, threshold=1)
    
    endpoint_log = '/api/v1/inform-severity-index/log/'
    df_log = acaps_data.read_or_get_csv(endpoint_log, indir, username, password, n_months=-1)    
    # in this case the time stamp is not the same for all the indicators, so a dict of time series works better than a dataframe
    # ts_log_dict[crisis][indicator] = time series
    ts_log_dict = create_ts_dictionary(df_log, evolving_crises)

    endpoint_country_log = '/api/v1/inform-severity-index/country-log/'
    df_country_log = acaps_data.read_or_get_csv(endpoint_country_log, indir, username, password, n_months=-1)
    ts_country_log_dict = create_ts_dictionary(df_country_log, evolving_crises, is_country=True)
    for cr_id in evolving_crises: # add Fatalities in all crises with other complexity indicators
        ts_country_log_dict[cr_id]['Fatalities in all crises'] = ts_log_dict[cr_id]['Fatalities in all crises']
    
    indicators = create_indicators_dictionary(ts_log_dict, evolving_crises)

    for cr_id in evolving_crises:
        for group in indicators[cr_id]:
            if len(indicators[cr_id][group]) ==0: continue
            fig = produce_plot_indicators(df[df['crisis_id']==cr_id], ts_log_dict[cr_id], indicators[cr_id][group])
            fig.update_layout(title_text=group + " indicators, "+cr_id)
            write_fig(fig, cr_id+'_indicators_'+group.lower().replace(' ','_'), outdir, do_png, do_pdf)

    # now look at the per-country information
    # which indicators to look at has been determined in the explorative analysis
    indicators_country = create_indicators_dictionary(ts_country_log_dict, evolving_crises, country_log=True)
    for cr_id in evolving_crises:
        for group in indicators_country[cr_id]:
            if len(indicators_country[cr_id][group]) ==0: continue
            fig = produce_plot_indicators(df[df['crisis_id']==cr_id], ts_country_log_dict[cr_id], indicators_country[cr_id][group])
            fig.update_layout(title_text='Country indicators, '+group +', '+cr_id)
            write_fig(fig, cr_id+'_country_indicators_'+group.lower().replace(' ','_'), outdir, do_png, do_pdf)
    
    return


    
def main():
    args = parse_args()
    # print(args.vis)
    plot_all = 'all' in args.vis
    if 'map' in args.vis or plot_all:
        plot_map_index(args.indir, args.outdir, args.png, args.pdf, args.username, args.password)
    if 'evolving-crises' in args.vis or plot_all:
        plot_evolving_crises(args.indir, args.outdir, args.png, args.pdf, args.username, args.password)
    if 'corr' in args.vis or plot_all:
        plot_correlation_with_access(args.indir, args.outdir, args.png, args.pdf, args.username, args.password)
    if 'indicators' in args.vis or plot_all:
        plot_indicators(args.indir, args.outdir, args.png, args.pdf, args.username, args.password)
    return


if __name__ == '__main__':
    main()
