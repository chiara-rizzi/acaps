---
title: Report 
link: report.html 
image: images/general/report.png
---

Report of the analysis showing an overview of the INFORM Severity Index,
a study of the deteriorating crises in the last 18 months,
and the correlation of the INFORM Severity Index with the Humanitarian Access Score. 
