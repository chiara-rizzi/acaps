---
layout: post
title:  "Report on INFORM Severity Index"
permalink: report.html
---
 
- [Introduction](#introduction)
- [Overview of the INFORM Severity Index](#overview-of-the-severity-index)
- [Q1: Deteriorating Crises](#q1-deteriorating-crises)
- [Q2: Indicators and Specific Examples](#q2-indicators-and-specific-examples)
    - [Indicators Considered](#indicators-considered)
    - [Mixed Migration Flows in Yemen](#mixed-migration-flows-in-yemen)
    - [Northwest Banditry in Nigeria](#northwest-banditry-in-nigeria)
    - [Cabo Delgado Islamist Insurgency](#cabo-delgado-islamist-insurgency)
- [Q3: Correlation with Humanitarian Access Score](#q3-correlation-with-humanitarian-access-score)
- [Conclusion](#conclusion)

## Introduction

The scope of this analysis is to present the evolution of ongoing international crises during the past 18 months, focusing primarily on the [INFORM Severity Index](https://www.acaps.org/methodology/severity) and, in the last paragraph, on the [Humanitarian Access Score](https://www.acaps.org/methodology/access). The Severity Index is a model that quantifies the overall severity of a crisis by evaluating and combining several different indicators, with the ultimate goal of enabling fair comparisons between different contexts and situations, and supporting evidence-based decision making (e.g., need for humanitarian intervention, resource allocation). The Humanitarian Access Score is instead designed to measure the challenge associated with humanitarian interventions in a given context, again taking into account a list of parameters that are combined to obtain a quantitative assessment.

The analysis begins with an overview of the evolution of the INFORM Severity Index of all crises in the past 18 months, presented in the form of an interactive map. After this I show a selection of five crises where significant variations of Severity Index have been observed, adding more details about individual indicators for three of them. Lastly, I present the overall correlation between the INFORM Severity Index and the Humanitarian Access Score for all cases where both values are available, highlighting individual cases with high and low correlation.

## Overview of the Severity Index

The animation below provides an overview of the geographical distribution of the active crises and their INFORM Severity Index, together with their time evolution during the past 18 months. 
By zooming on the map it is possible to focus on specific regions, and by passing the pointer on each point one can easily retrieve the main information about each crisis. While this is already helpful to visualize the general situation, a more refined analysis can be implemented to extract specific highlights, as further described below.

{% include_relative figures/map.html %}

## Q1: Deteriorating Crises

A primary goal of the analysis is to identify the crises where a significant variation of the severity has occurred over the past 18 months. To do so, I am showing below the evolution of the INFORM Severity Index for the subset of crises where the excursion between the maximum and the minimum value of the score has been larger than a threshold (chosen to be equal to 1). While a priori this criterion could identify crises that have either improved or deteriorated, in practice one can see from the figure that the crises with such large Severity Index excursion have all evolved negatively. The figure is again interactive, and it is possible to click on the legend items to select/deselect which lines to display. The lines are drawn with different styles simply to be able to visualize them better when they overlap. 

{% include_relative figures/deteriorating_crises.html %}

Upon inspection of the figure, we can see that the crisis that has shown the worst increase of severity is YEM002 (Mixed Migration Flows in Yemen), with Severity Index evolving from a minimum of 2.4 in June 2021 to a maximum of 4.6 from September 2021 onwards. In Nigeria, NGA003 (Middle Belt Conflict) and NGA007 (Northwest Banditry) have both shown a progressive increase of severity over the past 18 months, peaking at Severity Index 3.5 and 3.9 from August 2021. The Severity Index of MOZ004 (Cabo Delgado Islamist Insurgency) has been fairly stably at 3.5 during 2021 (with just one month at 3.6), but it has significantly increased compared to 2020, when it was as low as 2.1 in November and December. Lastly, MMR004 (Post-coup Violence in Myanmar) is included in the list as it is a new crisis that erupted in early 2021, with first Severity Index logged in June (2.1) and rising to 3.5 from July up to now.

## Q2: Indicators and Specific Examples

The figures below present the detail of the individual indicators that are used to compute the INFORM Severity Index, for three crises that have shown a significant deterioration over the past 18 months. In particular, I chose to focus on YEM002 (Mixed migration flows in Yemen), NGA007 (Northwest Banditry in Nigeria) and MOZ004 (Cabo Delgado Islamist Insurgency in Mozambique), for which the overall Severity Index evolution in the past months has been discussed in the previous paragraph.

#### Indicators Considered 

For all the three selected crises, I have considered all the indicators available in the logs (`inform-severity-index/log/` and `inform-severity-index/country-log/`. Not all the indicators are always available, and sometimes they are not updated with sufficient frequency to be relevant for this study, which considers only the last 18 months. The lack of entries is particularly common for the country-level indicators, that are used in the computation of the crisis complexity. Hence, for the analysis below I consider only the indicators that are available and with entries in the period of time under exam. To guide the interpretation of the results, I group the indicators in three categories according to the associated dimension in the computation of the Severity Index, namely the impact of the crisis, the conditions of the people, and the complexity of the crisis. 

**Impact of the crisis**

In the dimension 'impact of the crisis', the indicators that are most relevant for the cases considered are:
- Landmass
- People exposed 
- People affected 
- People displaced 
- Fatalities 

**Conditions of people**

The indicators related to the conditions of people are: 
- Level 1: people facing none/minimal humanitarian conditions 
- Level 2: people facing stressed humanitarian conditions 
- Level 3: people facing moderate humanitarian conditions 
- Level 4: people facing severe humanitarian conditions 
- Level 5: people facing extreme humanitarian conditions 
- People in need (level 3-5)


**Complexity of the crisis**

For the complexity of the crisis, the key indicators are:
- Fatalities in all crises: Total people killed by conflict in the country where the crisis is ongoing 
- Income Gini coefficient 
- Conflict intensity 

**Notes**

As indicated in the INFORM Severity Index methodology note, indicators such as illnesses, injured, physical damages, economic losses are currently not included in the model due to the lack of data coverage. 

#### Mixed Migration Flows in Yemen

With the exception of MMR004, which is a new crisis developed in 2021, the crisis that has shown the worst deterioration over the past 18 months according to the INFORM Severity Index is the one related to the mixed migration flows in Yemen (YEM002). The evolution of the values of single indicators is shown in the figures below. As previously discussed, one should note that not all indicators are updated at the same time, and some of them only show a small number of entries. Overall, however, it is clear that based on their evolution one indeed expects a significant increase of the INFORM Severity Index, as it is indeed observed.

{% include_relative figures/YEM002_indicators_impact.html %}

All impact indicators have shown a substantial degradation in September 2021, consistently with the sharp increase of INFORM Severity Index.

{% include_relative figures/YEM002_indicators_conditions.html %}
The number of people classified as living in 'condition 1' has dropped dramatically from June to September 2021, while all other condition indicators have seen a significant increase over the same period. 

{% include_relative figures/YEM002_country_indicators_complexity.html %}

Most country indicators are instead not available for the peak period of the INFORM Severity Index (i.e., from September 2021 onwards). Still, the previous indicators are clearly sufficient to confirm that the overall severity of the crisis has increased dramatically.

#### Northwest Banditry in Nigeria

Another crisis that has deteriorated in the last few months is NGA007 (Northwest Banditry in Nigeria), and the associated values of the single indicators are shown below. Importantly, after the Severity Index has reached a peak of 3.9 in August 2021, no new indicator data has been made available, hence the Severity Index has remained stable. 

{% include_relative figures/NGA007_indicators_impact.html %}

The increase of the Severity Index to 3.9 in August 2021 is driven by the number of affected and exposed people, while no entry was available for the other indicators for this month.

{% include_relative figures/NGA007_indicators_conditions.html %}

Similarly, the August peak corresponds to a substantial increase of the number of people in condition level 3. The lack of people in condition levels 4 and 5 is coherent with the fact that the overall Severity Index of this crisis is lower compared to YEM002.

{% include_relative figures/NGA007_country_indicators_complexity.html %}

As in the case of YEM002, the country indicators are not available for the peak period of the INFORM Severity Index (i.e., from August 2021 onwards).

#### Cabo Delgado Islamist Insurgency

As described above, the Severity Index of the MOZ004 crisis has increased significantly between the end of 2020 and January 2021, while afterwards it has remained basically stable up to now. The figures below show the breakdown of single indicators according to the same classification employed for the previous cases. Interestingly, despite the overall stability of the Severity Index during 2021, the individual indicators have varied by non-negligible amounts, all while being, of course, generally worse compared to 2020. 

{% include_relative figures/MOZ004_indicators_impact.html %}

In general, the impact indicators shown in the above figure have increased in 2021 compared to 2020, but not exactly at the same time.

{% include_relative figures/MOZ004_indicators_conditions.html %}

The large increase of Severity Index from December 2020 (2.1) to January 2021 (3.5) is initially driven by an increase of people in condition levels 3 and 4, while later it is followed by a large increase of people in condition level 2 and, eventually, also in condition level 1. As for the impact indicators, during 2021 the variations of the number of people in condition levels 1 to 4 were not reflected by a significant change of Severity Index. Lastly, the index linked to the condition level 5 has remained at 0 for the full period under exam, unlike the case of the YEM002 crisis (which, indeed, has a higher Severity Index).

{% include_relative figures/MOZ004_country_indicators_complexity.html %}

Coherently with the Severity Index, the Conflict Intensity indicator (HIIK) has increased from 2020 to 2021, whereas the Income Gini coefficient has remained stable (although, for both cases, only two logged entries are available). The indicator of the fatalities in all crises for Mozambique has a very similar trend compared to the reported fatalities of the MOZ004 crisis (visible among the impact indicators above), which means that this crisis has dominated the number of fatalities in the country.


## Q3: Correlation with Humanitarian Access Score

The [Humanitarian Access Score](https://www.acaps.org/methodology/access) provides an indication of the challenges related to
humanitarian access, and it is hence based on a different set of parameters compared to the INFORM Severity Index.

However, when considering all crises of the last 18 months for which both the INFORM severity index and the ACCESS score are available,
it is clear that the two values are strongly correlated (the Pearson correlation coefficient is equal to 0.7). The correlation is visible in the interactive figure below, where the opacity of the markers is proportional to the density of the entries at each given point. 

{% include_relative figures/correlation.html %}

Such correlation is not surprising, since realistically the same root causes that lead to a critical crisis can often complicate humanitarian access (for example, the presence of armed conflict). This is clearly the case for the crises with both high Severity Index and high Humanitarian Access Score, such as the Syrian Conflict, the Conflict in Yemen, the Complex Crisis in Ethiopia and others. At the same time, it is reasonable that crises with low Severity Index can often pose less challenges in terms of humanitarian access. 

Still, the figure also highlights the presence of "outliers", i.e., crises with either low Severity Index and high Humanitarian Access Score, or vice versa. A first example is the crisis linked to Mixed Migration Flows in Somalia, where the Severity Index is relatively low (1.9 in June 2021) but the Humanitarian Access Score is the highest (5 in the same month). On the opposite end one can cite the Rohingya Regional Crisis, which in September 2020 had Severity Index 3.4 but Humanitarian Access Score 0. 

It is also to be noted that the humanitarian access is one of the components of the dimension 'complexity of the crisis', used to compute the INFORM Severity Index (the overall crisis complexity is weighted 30% in the average used to compute the severity index). While the computation of the Access Score and the 'complexity of the crisis' component is different, we do expect the correlation. This is visible in the figure below. 

{% include_relative figures/correlation-comp.html %}

From this figure, we can clearly see that the component of the INFORM Severity Score which
is most correlated with the ACCESS score is the complexity of the crisis. More quantitatively,
we can see in the figure below the Pearson correlation coefficient for the various indicators,
which confirms the statement above: the ACCESS Score has a correlation of 77% with the complexity of the crisis,
while the correlation with the impact of the crisis and the condition of the affected people is lower, approximately 50%. 

{% include_relative figures/correlation-map.html %}

Lastly, it is interesting to examine the correlation with the components of the complexity dimensions, namely 
diversity of the groups affected, humanitarian access, rule of law, safety and security, and social cohesion.
This is shown in the figure below. 

{% include_relative figures/correlation-map-complexity.html %}

Indeed, the Humanitarian Access Score has a 97% correlation with the humanitarian access component of the
computation of the INFORM Security Index. 

## Conclusion

In this report, I have presented an analysis of the INFORM Severity Index for ongoing humanitarian crises over the past 18 months.
After presenting an overview of the geographical distribution of all crises,
I have then identified 5 of them for which the excursion of the Severity Index has been larger than 1 in the past 18 months,
finding that all of them have evolved negatively. For three of these crises,
the variations of the inputs to the indicators have been studied in detail,
identifying the underlying reasons for the increase in INFORM Severity Index.
Lastly, the correlation of the INFORM Severity Index with the Humanitarian Access Score is found to be 70% when analyzing the crises of the past 18 months.
While there is some level of correlation between the Access Score and all the dimensions of the INFORM Severity Index,
the largest correlation is found with the complexity dimension, 
and in particular (as expected) for the component of the complexity which is itself a measure of the humanitarian access.

