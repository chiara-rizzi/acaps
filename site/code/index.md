---
layout: post
title:  "Code and Analysis Approach"
permalink: code.html
---


The code for this analysis (and for this website) is completely contained in [this gitlab repository](https://gitlab.com/chiara-rizzi/acaps). 

## Quick Start 

#### Get the code

```bash
git clone https://gitlab.com/chiara-rizzi/acaps.git
cd acaps
```

#### Environment

The first time you setup the environment:

Here are the instructions to setup the environment with the [conda](https://docs.conda.io/en/latest/)
package manager. If you can find the Miniconda installation instructions [here](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). 

```bash
conda env create -f acaps_conda.yml
conda activate acaps
```

The creation of the environment might take a few minutes. 

Every time you login:
```bash
conda activate acaps
```

#### Get the data

```bash
cd scripts/data
python get_all_data.py -o ../../data/raw/ -c all_endpoints.json
cd ../../
```

You can pass your username and password as additional arguments (with the options `-u` and `-p`),
or you will be asked to input them while the script runs. 

#### Run the analysis and the visualizations

This will run the analysis on the downloaded csv data and place the output visualizations
in the website report folder

```bash
cd scripts/analysis
python analysis.py -v all -i ../../data/raw/ -o ../../site/report/figures/ --png
```

#### Automatic deployment of the report website

The website is deployed automatically at evey commit involving a change to the `site` folder, thanks
to the [CI setup](https://gitlab.com/chiara-rizzi/acaps/-/blob/master/.gitlab-ci.yml)


## Analysis Approach

#### Explorative Analysis

The first explorative analysis has been carried out using jupyter notebooks.
This step is not meant to produce the final results, but only to have an easy way to interact with
the dataset, in order to understand the main features. 

#### Structured Analysis

Once the main features of the data have been understood, the analysis is carried out with python packages
and modules. Some highlights:

- All the functions written to inteact with the ACAPS API are grouped in the [acaps](https://gitlab.com/chiara-rizzi/acaps/-/tree/master/acaps)
package, and in particular in [acaps_data.py](https://gitlab.com/chiara-rizzi/acaps/-/blob/master/acaps/acaps_data.py).
- The analysis and and production of the visualizations is performed by the script [analysis.py](https://gitlab.com/chiara-rizzi/acaps/-/blob/master/scripts/analysis/analysis.py).
  * Most of the visualizations are performed with [plotly](https://plotly.com/python/), a python open source graphic library.
    This library was chosed having in mind the format of the report: a static website. Plotly allows to create interactive
    visualizations and export them to html, which can be easily included in the report.
  * The data manipulation necessary for the analysis is done with [pandas](https://pandas.pydata.org/). 
- The analysis script will try to read the csv file, and if not available it will automatically download it (asking for the username and
password). It might be convenient to download all the necessary data in one go, and this can be done with the
[get_all_data.py](https://gitlab.com/chiara-rizzi/acaps/-/blob/master/scripts/data/get_all_data.py) script. 

#### Report and Website

The report for this project is written in the form of a static website, generated with [jekyll](https://jekyllrb.com/). In the report, the visualizations are integrated directly as html files. Please note that the correct visualization of the site requires an internet connection in order to load the plotly.js library. It is possible to produce html files that are self-contained and do not require an internet connection at the price of heavier html files. 

The website is automatically deployed every time a change related to the `site` repository is pushed to gitlab. This is done through the [.gitlab-ci.yml](https://gitlab.com/chiara-rizzi/acaps/-/blob/master/.gitlab-ci.yml) file. 

## Repository Structure

The repository structure follows the analysis approach outlined above. 

```
├── README.md
├── acaps ------------------------- package to read and manipulate ACAPS data
│   ├── __init__.py           
│   └── acaps_data.py
├── data                      
│   ├── processed  ---------------- intermediate data (if needed)             
│   └── raw ----------------------- files from ACAPS API converted to csv
├── notebooks --------------------- notebooks for exploratory analysis
├── scripts 
│   └── analysis ------------------ scripts for analysis
│       └── analysis.py ----------- main script for analysis and visualization
│   └── data ---------------------- scripts to get data
│       └── get_all_data.py ------- retrieve all the necessary data
└── site -------------------------- website
    ├── _config.yml
    ├── code 
    │   └── index.md -------------- main page for description of code
    ├── images 
    ├── index.md
    ├── report
    │   ├── figures
    │   └── index.md -------------- main page for report
    └── test_rendering.sh
```



