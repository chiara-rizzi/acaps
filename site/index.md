---
#
# By default, content added below the "---" mark will appear in the home page
# between the top bar and the list of recent posts.
# To change the home page layout, edit the _layouts/home.html file.
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
#
layout: home
---

# Analysis of INFORM Severity Index

  {%- if site.index.size > 0 -%}
  {%- for project in site.index -%}
  {% include projectitem.html %}
  {%- endfor -%}
  {%- endif -%}


{% include_relative report/figures/map.html %}

<br/><br/>

