# ACAPS INFORM Severity Index

This repository contains an analysis of the 
[INFORM Severity Index](https://www.acaps.org/methodology/severity). 
In particular, I provide the code to get the data, analyze it, and visualize the results. 
The results are automatically deployed on a website.

## Information
- The code is documented [here](https://crizzi.web.cern.ch/inform-severity-index/code.html)
- The report for the analysis is available [here](https://crizzi.web.cern.ch/inform-severity-index/report.html)

## Quick Start

### Get the code

```bash
git clone https://gitlab.com/chiara-rizzi/acaps.git
cd acaps
```

### Environment

The first time you setup the environment.
Here are the instructions to setup the environment with the [conda](https://docs.conda.io/en/latest/)
package manager. If you can find the Miniconda installation instructions [here](https://conda.io/projects/conda/en/latest/user-guide/install/index.html). 

```bash
conda env create -f acaps_conda.yml
conda activate acaps
```

The creation of the environment might take a few minutes. 

Every time you login:
```bash
conda activate acaps
```

### Get the data

```bash
cd scripts/data
python get_all_data.py -o ../../data/raw/ -c all_endpoints.json
cd ../../
```

### Run the analysis and the visualizations

This will run the analysis on the downloaded csv data and place the output visualizations
in the website report folder

```bash
cd scripts/analysis
python analysis.py -v all -i ../../data/raw/ -o ../../site/report/figures/ --png
```

### Automatic deployment of the report website

The website is deployed automatically at every commit involving a change to the `site` folder, thanks
to the [CI setup](.gitlab-ci.yml)






